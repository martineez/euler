object Euler001 {
  // https://projecteuler.net/problem=1
  // If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
  // The sum of these multiples is 23.
  // Find the sum of all the multiples of 3 or 5 below 1000.

  def solution1 = {
    var ∑ = 0
    List.range(1, 1000).filter(i => i % 3 == 0 || i % 5 == 0).foreach(∑ += _)
    ∑
  }

  def solution2 = {
    (1 until 1000).filter(i => i % 3 == 0 || i % 5 == 0).foldLeft(0)(_ + _)
  }

  def solution3 = {
    (1 until 1000).view.filter(i => i % 3 == 0 || i % 5 == 0).sum
  }

  def solution4 = {
    var ∑ = 0;
    ∑ += (3 + 999) * 333 / 2; // Add all numbers divides by 3
    ∑ += (5 + 995) * 199 / 2; // Add all numbers divides by 5
    ∑ -= (15 + 990) * 66 / 2; // remove all numbers divides by 15
    ∑
  }

  def main(args: Array[String]) {
    assert(233168 == solution1)
    assert(233168 == solution2)
    assert(233168 == solution3)
    assert(233168 == solution4)
  }
}
