object Euler003 {
  // The prime factors of 13195 are 5, 7, 13 and 29.
  // What is the largest prime factor of the number 600851475143 ?

  def isPrime(p: Int) {
    //    (2 until Math.sqrt(p))(p => p % i == 0)
  }

  def factors(n: Long): List[Long] =
    (2 to math.sqrt(n).toInt).find(n % _ == 0)
      .map(i => i.toLong :: factors(n / i)).getOrElse(List(n))

  def solution1 = factors(600851475143L).last

  def main(args: Array[String]) {
    assert(6857 == solution1)
  }
}
